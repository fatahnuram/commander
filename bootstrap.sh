#!/usr/bin/env bash

# check user priv
if [[ $(whoami) == root ]]; then
    # root user
    true
else
    # non-root user
    # force exit
    echo
    echo "Need root privileges!"
    echo
    exit 1
fi

# check args num
if [[ $# -ne 0 ]]; then
    # args inserted
    # force exit
    echo
    echo "No need any args to run this!"
    echo
    exit 1
fi

# check if python3 is installed
echo
echo "Getting python3 binary location.."
TEST=$(python3 --version &> /dev/null)
if [[ ${TEST} -eq 0 ]]; then
    # python3 is installed
    # get binary
    INTERPRETER_LOC=$(which python3)
    echo "Found in ${INTERPRETER_LOC}"
    echo
else
    # python3 is not installed
    # force exit
    echo
    echo "Python 3 is not installed!"
    echo
    exit 1
fi

# install process
# set home directory
CMDER_HOME='/opt/commander'
# copy whole directory into home directory
echo "Copying files into ${CMDER_HOME}.."
cp -r . ${CMDER_HOME}
# replace python binary in service file
echo "Configuring service.."
sed -i s,python3,${INTERPRETER_LOC}, ${CMDER_HOME}/commander.service
# move service file
mv ${CMDER_HOME}/commander.service /etc/systemd/system/
# reload daemon service
systemctl daemon-reload
# start service
echo "Starting service.."
systemctl start commander
# enable service
systemctl enable commander
echo
echo "Done!"
echo