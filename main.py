#!/usr/bin/env python

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from requests import get
from socket import gethostname
from subprocess import run
import os
import logging
import cred

# setup logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger()

# global variabales
shutdown_msg = None
shutdown_reply_msg = None
restart_msg = None
restart_reply_msg = None

def start(bot, update):
    # compose message to send
    msg = "Hi!"
    msg += "\nPlease use /help to start using me!"
    # log activity
    logger.info("/start clicked!")
    # send message
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def help(bot, update):
    # compose message to send
    msg = "/help - show this help message"
    msg += "\n/detail - show device details"
    msg += "\n/pwd - show current directories"
    msg += "\n/whoami - show current user"
    msg += "\n/shutdown - shutdown active device"
    msg += "\n/restart - restart active device"
    msg += "\n"
    msg += "\nNotes: the bot's still under development.."
    # log activity
    logger.info("/help clicked!")
    # send message
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def detail(bot, update):
    # get information
    try:
        ip = get('http://ipinfo.io/ip').text
    except:
        ip = "unknown (error occured)"
    try:
        name = gethostname()
    except:
        msg = "Unknown error occured"
    else:
        msg = name + " --> " + ip
    # log activity
    logger.info("/detail clicked!")
    logger.info("msg: " + msg)
    # send msg
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def pwd(bot, update):
    # get information
    current_dir = os.popen("pwd").read()
    # compose message to send
    msg = str(current_dir)
    # log activity
    logger.info("/pwd clicked!")
    logger.info(msg)
    # send message
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def whoami(bot, update):
    # get information
    current_user = os.popen("whoami").read()
    # compose message to send
    msg = str(current_user)
    # log activity
    logger.info("/whoami clicked!")
    logger.info(msg)
    # send message
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def shutdown(bot, update):
    # log activity
    logger.info("/shutdown clicked!")
    # send confirmation message
    global shutdown_msg
    msg = "Are you sure want to shutdown your device?"
    msg += "\n"
    msg += "\nReply this message with 'yes' to confirm, or 'no' to cancel command.."
    shutdown_msg = bot.send_message(chat_id=update.message.chat_id, text=msg)

def shutdown_exec(bot, update):
    # log activity
    logger.info("shutdown confirmed, shutting down..")
    # send message
    msg = "Shutting down.."
    bot.send_message(chat_id=update.message.chat_id, text=msg)
    # shutdown server
    run(["poweroff"])

def shutdown_cancel(bot, update):
    # cancel confirmation chat
    global shutdown_msg
    shutdown_msg = None
    # log activity
    logger.info("shutdown cancelled")
    # send message
    msg = "Shutdown cancelled"
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def restart(bot, update):
    # log activity
    logger.info("/restart clicked!")
    # send confirmation message
    global restart_msg
    msg = "Are you sure want to restart your device?"
    msg += "\n"
    msg += "\nReply this message with 'yes' to confirm, or 'no' to cancel command.."
    restart_msg = bot.send_message(chat_id=update.message.chat_id, text=msg)

def restart_exec(bot, update):
    # log activity
    logger.info("restart confirmed, restarting..")
    # send message
    msg = "Restarting.."
    bot.send_message(chat_id=update.message.chat_id, text=msg)
    # restart server
    run(["reboot"])

def restart_cancel(bot, update):
    # cancel confirmation chat
    global restart_msg
    restart_msg = None
    # log activity
    logger.info("restart cancelled")
    # send message
    msg = "Restart cancelled"
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def reply(bot, update):
    # filter the reply chat
    global shutdown_msg
    global shutdown_reply_msg
    global restart_msg
    global restart_reply_msg

    # check if replying to shutdown msg
    if update.message.reply_to_message is not None and update.message.reply_to_message == shutdown_msg:
        # check reply chat
        if update.message.text == "yes":
            # exec shutdown
            shutdown_exec(bot, update)
        elif update.message.text == "no":
            # cancel confirmation chat
            shutdown_cancel(bot, update)
        else:
            # prompt accepted answer
            shutdown_reply_msg = bot.send_message(chat_id=update.message.chat_id, text="Only 'yes' or 'no' accepted")
    # check if replying to restart msg
    elif update.message.reply_to_message is not None and update.message.reply_to_message == restart_msg:
        # check reply chat
        if update.message.text == "yes":
            # exec restart
            restart_exec(bot, update)
        elif update.message.text == "no":
            # cancel confirmation chat
            restart_cancel(bot, update)
        else:
            # prompt accepted answer
            restart_reply_msg = bot.send_message(chat_id=update.message.chat_id, text="Only 'yes' or 'no' is accepted")
    # check if replying to reply msg
    elif update.message.reply_to_message is not None and (update.message.reply_to_message == shutdown_reply_msg or update.message.reply_to_message == restart_reply_msg):
        bot.send_message(chat_id=update.message.chat_id, text="Please reply my chat before, not that chat..")
    # not replying to any chat, send help message
    else:
        bot.send_message(chat_id=update.message.chat_id, text="Please use /help to start using me!")

def main():
    # get updater with token
    updater = Updater(token=cred.token)
    # get dispatcher from updater
    dispatcher = updater.dispatcher

    # register command handlers
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('help', help))
    dispatcher.add_handler(CommandHandler('detail', detail))
    dispatcher.add_handler(CommandHandler('pwd', pwd))
    dispatcher.add_handler(CommandHandler('whoami', whoami))
    dispatcher.add_handler(CommandHandler('shutdown', shutdown))
    dispatcher.add_handler(CommandHandler('restart', restart))

    # register message handlers
    dispatcher.add_handler(MessageHandler(Filters.text, reply))

    # start the bot
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()