# commander

Your Telegram bot is your linux remote commander

## Notes

This repository is now **working**. However, this repository is still **in development phase**.

## How to run

Install dependencies as root

```bash
sudo pip install -r requirements.txt
```

Provide your bot's token into file named `cred.py`

```bash
echo "token = 'YOUR_BOT_TOKEN_HERE'" > cred.py
```

Make sure all `*.sh` files are executable

```bash
chmod +x *.sh
```

Bootstrap to run

```bash
sudo ./bootstrap.sh
```

## LICENSE

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png "WTFPL")

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

Copyright (C) 2018 Fatah Nur Alam Majid <fatahnuram@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.